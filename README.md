This project contains the source code of the JUnit 3.8.2 framework.  
Automated builds are done using Maven. Running the build system from the command line:
```
$ mvn compile
$ mvn package
``` 
Executing the test cases and calculating code coverage from the command line:
```
$ mvn test
``` 
The project uses JUnit 4.11 for testing and JaCoCo 0.8.5 for calculating code coverage. 
`mvn package` also executes the tests automatically along with calculating the code coverage. 

[![pipeline status](https://gitlab.com/sbs39/hbv204m/badges/master/pipeline.svg)](https://gitlab.com/sbs39/hbv204m/-/commits/master)
 
[![coverage report](https://gitlab.com/sbs39/hbv204m/badges/master/coverage.svg)](https://gitlab.com/sbs39/hbv204m/-/commits/master)
 
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=sbs39_hbv204m&metric=alert_status)](https://sonarcloud.io/dashboard?id=sbs39_hbv204m)