package is.hi.cs.hbv204m;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for class Assert.
 */
public class AssertTest 
{
    @Test(expected = junit.framework.AssertionFailedError.class)
    public void testFail(){
    	junit.framework.Assert.fail();
    }

    @Test(expected = junit.framework.AssertionFailedError.class)
    public void testFailSame(){
    	junit.framework.Assert.failSame(null);
    }
    
    @Test(expected = junit.framework.AssertionFailedError.class)
    public void testAssertTrue(){
    	junit.framework.Assert.assertTrue(false);
    }
    
    @Test(expected = junit.framework.AssertionFailedError.class)
    public void testAssertFalse(){
    	junit.framework.Assert.assertFalse(true);
    }
}
